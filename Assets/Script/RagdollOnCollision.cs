using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RagdollOnCollision : MonoBehaviour
{
    private const string PLAYER_TAG = "Player";

    private void Start()
    {
        SetKinematic(true);
    }

    private void SetKinematic(bool newValue)
    {
        Rigidbody[] bodies = GetComponentsInChildren<Rigidbody>();
        foreach (Rigidbody rb in bodies)
        {
            rb.isKinematic = newValue;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(PLAYER_TAG))
        {
            SetKinematic(false);
        }
    }

}
