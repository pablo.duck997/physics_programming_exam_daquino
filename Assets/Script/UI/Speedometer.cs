using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Speedometer : MonoBehaviour
{
    [SerializeField] private Rigidbody carRb;
    [SerializeField] private Text speedometerText;
    private float currentSpeed;

    void FixedUpdate()
    {
        PrintSpeed();
    }

    private void PrintSpeed()
    {
        currentSpeed = carRb.velocity.magnitude * 3.6f;
        speedometerText.text = ((Mathf.Round(currentSpeed)).ToString("000") + " KM/H");
    }
}
