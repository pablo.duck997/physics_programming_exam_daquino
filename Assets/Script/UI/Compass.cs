using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Compass : MonoBehaviour
{
    [SerializeField] private Transform player;
    [SerializeField] private Image compass;
    private float offset = 90f;
    private Vector3 direction;

    private void OnGUI()
    {
        SetCompass();
    }

    private void SetCompass()
    {
        direction.z = -player.eulerAngles.y - offset;
        compass.transform.localEulerAngles = direction;
    }
}
