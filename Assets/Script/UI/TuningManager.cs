using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TuningManager : MonoBehaviour
{
    #region SCENE_REFERENCES
    [SerializeField] private CarSettingsScriptable carData;
    [Header("Slider")]
    [SerializeField] private Slider speedSlider;
    [SerializeField] private Slider torqueSlider;
    [SerializeField] private Slider turboSlider;
    [SerializeField] private Slider liftSlider;
    [SerializeField] private Slider massSlider;
    [Header("Toggle")]
    [SerializeField] private Toggle frontWheelsToggle;
    [SerializeField] private Toggle rearWheelsToggle;
    [Header("Text")]
    [SerializeField] private Text speedText;
    [SerializeField] private Text torqueText;
    [SerializeField] private Text turboText;
    [SerializeField] private Text liftText;
    [SerializeField] private Text massText;

    private CarController controller = null;

    #endregion

    #region SETUP
    void Start()
    {
        controller = FindObjectOfType<CarController>();
        SetUpUI();
    }

    private void SetUpUI()
    {
        //slider setup
        if (speedSlider) 
        { speedSlider.minValue = carData.minMax_Speed.x; speedSlider.maxValue = carData.minMax_Speed.y; speedSlider.value = carData.maxSpeed;
        speedSlider.onValueChanged.AddListener(delegate { UpdateSpeedValue(speedSlider.value); }); }
        if (torqueSlider) 
        { torqueSlider.minValue = carData.minMax_TorqueForce.x; torqueSlider.maxValue = carData.minMax_TorqueForce.y; torqueSlider.value = carData.torqueForce;
        torqueSlider.onValueChanged.AddListener(delegate { UpdateTorqueValue(torqueSlider.value); }); }
        if (turboSlider) 
        { turboSlider.minValue = carData.minMax_TurboCoefficent.x; turboSlider.maxValue = carData.minMax_TurboCoefficent.y; turboSlider.value = carData.turboCoefficent;
        turboSlider.onValueChanged.AddListener(delegate { UpdateTurboValue(turboSlider.value); }); }
        if (liftSlider) 
        { liftSlider.minValue = carData.minMax_DownForce.x; liftSlider.maxValue = carData.minMax_DownForce.y; liftSlider.value = carData.liftCoefficient;
        liftSlider.onValueChanged.AddListener(delegate { UpdateLiftValue(liftSlider.value); }); }
        if (massSlider) 
        { massSlider.minValue = carData.minMax_CarMass.x; massSlider.maxValue = carData.minMax_CarMass.y; massSlider.value = carData.carMass;
        massSlider.onValueChanged.AddListener(delegate { UpdateMassValue(massSlider.value); }); }
        //toggle setup
        if (frontWheelsToggle) {frontWheelsToggle.isOn = carData.activeFrontWheels; 
        frontWheelsToggle.onValueChanged.AddListener(delegate { UpdateFrontWheels(frontWheelsToggle.isOn); }); }
        if (rearWheelsToggle) {rearWheelsToggle.isOn = carData.activeRearWheels; 
        rearWheelsToggle.onValueChanged.AddListener(delegate { UpdateRearWheels(rearWheelsToggle.isOn); }); }
        //text setup
        if (speedText) {speedText.text = carData.maxSpeed.ToString("F0");}
        if (torqueText) {torqueText.text = carData.torqueForce.ToString("F0");}
        if (turboText) {turboText.text = carData.turboCoefficent.ToString("F1");}
        if (liftText) {liftText.text = (-1f * carData.liftCoefficient).ToString("F0");}
        if (massText) {massText.text = carData.carMass.ToString("F0");}
    }
    #endregion

    #region I/O
    private void UpdateSpeedValue(float i_maxSpeed) { controller.SetMaxSpeed(i_maxSpeed); speedText.text = i_maxSpeed.ToString("F0"); }
    private void UpdateTorqueValue(float i_torque) { controller.SetTorqueForce(i_torque); torqueText.text = i_torque.ToString("F0"); }
    private void UpdateTurboValue(float i_turbo) { controller.SetTurboCoefficent(i_turbo); turboText.text = i_turbo.ToString("F1"); }
    private void UpdateMassValue(float i_mass) { controller.SetCarMass(i_mass); massText.text = i_mass.ToString("F0"); }
    private void UpdateLiftValue(float i_lift) { controller.SetDownForceCoefficent(i_lift); liftText.text = (-1f * i_lift).ToString("F0"); }
    private void UpdateFrontWheels (bool i_frontWheels) { controller.ActivateFrontWheels(i_frontWheels); CheckForActiveWheels(); }
    private void UpdateRearWheels (bool i_rearWheels) { controller.ActivateRearWheels(i_rearWheels); CheckForActiveWheels(); }

    private void CheckForActiveWheels() //one couple of wheels at least must be active
    { if (!controller.GetFrontWheelsStatus() && !controller.GetRearWheelsStatus()) { frontWheelsToggle.isOn = true; UpdateFrontWheels(true); } }
    #endregion

}
