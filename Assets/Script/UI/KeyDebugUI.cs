using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class KeyDebugUI : MonoBehaviour
{
    [SerializeField] private Color defaultColor;
    [SerializeField] private Color pressedColor;

    [SerializeField] private Image w_img;
    [SerializeField] private Image a_img;
    [SerializeField] private Image s_img;
    [SerializeField] private Image d_img;
    [SerializeField] private Image r_img;
    [SerializeField] private Image space_img;

    void Start()
    {
        SetColor(w_img, defaultColor);
        SetColor(a_img, defaultColor);
        SetColor(s_img, defaultColor);
        SetColor(d_img, defaultColor);
        SetColor(r_img, defaultColor);
        SetColor(space_img, defaultColor);
    }

    private void SetColor(Image img, Color color) { img.color = color; }

    private void OnGUI()
    {
        if (Input.GetKey(KeyCode.W)) { SetColor(w_img, pressedColor); } else { SetColor(w_img, defaultColor); }
        if (Input.GetKey(KeyCode.A)) { SetColor(a_img, pressedColor); } else { SetColor(a_img, defaultColor); }
        if (Input.GetKey(KeyCode.S)) { SetColor(s_img, pressedColor); } else { SetColor(s_img, defaultColor); }
        if (Input.GetKey(KeyCode.D)) { SetColor(d_img, pressedColor); } else { SetColor(d_img, defaultColor); }
        if (Input.GetKey(KeyCode.R)) { SetColor(r_img, pressedColor); } else { SetColor(r_img, defaultColor); }
        if (Input.GetKey(KeyCode.Space)) { SetColor(space_img, pressedColor); } else { SetColor(space_img, defaultColor); }
        if (Input.GetKey(KeyCode.Escape)) { Application.Quit(); }
    }
}
