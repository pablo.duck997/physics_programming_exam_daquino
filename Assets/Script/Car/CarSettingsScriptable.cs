using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "CarData", menuName = "ScriptableObjects/CarSettingsScriptableObject", order = 1)]
public class CarSettingsScriptable : ScriptableObject
{
    #region CAR_DEFAULT_VALUES
    [Space] 
    [Header("Car Default Values")] 

    public bool activeFrontWheels = true;
    public bool activeRearWheels = false;
    [Tooltip("Car rigid body mass")]
    public float carMass = 1850f;
    public float maxSpeed = 277f;
    public float maxSpeedBakward = 70f;
    public float turboStartSpeed = 100f;
    public float turboCoefficent = 2.5f;
    [Tooltip("This force will be applied to each active wheel")]
    public float torqueForce = 1200f;
    [Tooltip("This force will be applied to each wheel")]
    public float breakForce = 1000f;
    public float maxSteerAngle = 30f;
    [Tooltip("Rigid body drag default value override")]
    public float rbDragDefault = 0f;
    [Tooltip("Rigid body drag value during breaks")]
    public float rbDragMalus = 0.6f;
    [Tooltip("A negative value will act as down force")]
    public float liftCoefficient = -35f;
    public float breakLightIntensity = 2f;
    #endregion

    #region UI_LIMITATIONS
    [Space] 
    [Header("UI limitations")] 
    [Tooltip("Set-up minum and max values for UI slider")]
    public Vector2 minMax_CarMass = new Vector2(1600f, 2500f);
    [Tooltip("Set-up minum and max values for UI slider")]
    public Vector2 minMax_Speed= new Vector2(220f, 370f);
    [Tooltip("Set-up minum and max values for UI slider")]
    public Vector2 minMax_TurboCoefficent = new Vector2(1f, 3.5f);
    [Tooltip("Set-up minum and max values for UI slider")]
    public Vector2 minMax_TorqueForce = new Vector2(1000f, 3000f);
    [Tooltip("Set-up minum and max values for UI slider")] 
    public Vector2 minMax_DownForce = new Vector2(-50f, 10f);
    #endregion

}
