using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LiftCar : MonoBehaviour
{
    //facilitate long jumps
    private const string PLAYER_TAG = "Player";
    float currentCoefficent = -35f;
    private bool check = false;
    WaitForSeconds wait = new WaitForSeconds(5f);
    CarController controller = null;

    private void OnTriggerEnter(Collider other)
    {
        if ((other.CompareTag(PLAYER_TAG))&&(!check))
        {
            controller = FindObjectOfType<CarController>();
            currentCoefficent = controller.GetDownForceCoefficent();
            StartCoroutine(nameof(Lift));
            check = true;
        }
    }

    private IEnumerator Lift()
    {
        controller.SetDownForceCoefficent(10f);
        yield return wait;
        controller.SetDownForceCoefficent(currentCoefficent);
        check = false;
    }
}
