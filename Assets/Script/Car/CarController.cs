using System;
using UnityEngine;

public class CarController : MonoBehaviour
{
    #region CONSTANT_PARAMETERS
    private const string HORIZONTAL = "Horizontal";
    private const string VERTICAL = "Vertical";
    #endregion

    #region INPUT
    private float horizontalInput;
    private float verticalInput;
    private float currentSteerAngle;
    private float currentSpeed;
    public float currentbreakForce;
    private bool isBreaking;
    private bool resetPosition;
    private bool isGoingBackWard; 
    private Vector3 localVelocity;
    #endregion

    #region CUSTOMIZABLE_PARAMETERS
    private bool activeFrontWheels = true;
    private bool activeRearWheels = false;
    private float maxSpeed = 277f;
    private float maxSpeedBakward = 70f;
    private float turboStartSpeed = 100f;
    private float turboCoefficent = 2.5f;
    private float torqueForce = 1200f;
    private float breakForce = 1000f;
    private float maxSteerAngle = 30f;
    private float rbDragDefault = 0f;
    private float rbDragMalus = 0.6f;
    private float liftCoefficient = -35f;
    private float breakLightIntensity = 2f;
    #endregion

    #region SCENE_REFERENCES
    [Space] //Creates a space inside of the unity editor
    [Header("Scene References")] //Creates a header inside of the unity editor

    [SerializeField] private WheelCollider frontLeftWheelCollider;
    [SerializeField] private WheelCollider frontRightWheelCollider;
    [SerializeField] private WheelCollider rearLeftWheelCollider;
    [SerializeField] private WheelCollider rearRightWheelCollider;

    [SerializeField] private Transform frontLeftWheelTransform;
    [SerializeField] private Transform frontRightWheeTransform;
    [SerializeField] private Transform rearLeftWheelTransform;
    [SerializeField] private Transform rearRightWheelTransform;

    [SerializeField] private Transform spawnPoint; //@todo: refactor

    [Tooltip("Car rigid body")]
    [SerializeField] private Rigidbody rb;
    [SerializeField] private Light[] breakLights;
    [SerializeField] private ParticleSystem[] breakSmokeParticles;
    #endregion

    #region SETUP_CAR_DATA

    [SerializeField] private CarSettingsScriptable carData;

    private void Awake()
    {
        GetCarDataSettings();
        transform.position = spawnPoint.position;
        transform.rotation = spawnPoint.rotation;
    }

    private void GetCarDataSettings()
    {
        activeFrontWheels = carData.activeFrontWheels;
        activeRearWheels = carData.activeRearWheels;
        maxSpeed = carData.maxSpeed;
        maxSpeedBakward = carData.maxSpeedBakward;
        turboStartSpeed = carData.turboStartSpeed;
        turboCoefficent = carData.turboCoefficent;
        torqueForce = carData.torqueForce;
        breakForce = carData.breakForce;
        maxSteerAngle = carData.maxSteerAngle;
        rbDragDefault = carData.rbDragDefault;
        rbDragMalus = carData.rbDragMalus;
        liftCoefficient = carData.liftCoefficient;
        breakLightIntensity = carData.breakLightIntensity;
        rb.mass = carData.carMass;
    }

    #endregion

    #region I/O
    private void FixedUpdate()
    {
        GetInput();
        HandleMotor();
        HandleSteering();
        CheckRotationReset();
        UpdateWheels();
        UpdateDownForce();
        UpdateDirection();
        UpdateBreakEffects();
    }

    private void GetInput()
    { 
        horizontalInput = Mathf.Clamp(Input.GetAxis(HORIZONTAL), -1, 1);
        verticalInput = Mathf.Clamp(Input.GetAxis(VERTICAL), -1, 1);
        isBreaking = Input.GetKey(KeyCode.Space);
        resetPosition = Input.GetKey(KeyCode.R);
        currentSpeed = rb.velocity.magnitude * 3.6f;
        localVelocity = transform.InverseTransformDirection(rb.velocity);
    }

    private void HandleMotor()
    {
        if (isGoingBackWard)
        {
            if (currentSpeed <= maxSpeedBakward)
            {
                if (currentSpeed < turboStartSpeed) ApplyTorque(verticalInput * torqueForce, rbDragDefault);
                    else ApplyTorque(verticalInput * (torqueForce * turboCoefficent), rbDragDefault);
            }
            else ApplyTorque(0f, rbDragMalus);
        }
        else
        {
            if (currentSpeed <= maxSpeed) ApplyTorque(verticalInput * torqueForce, rbDragDefault);
            else ApplyTorque(0f, rbDragMalus);
        }
        
        currentbreakForce = isBreaking ? breakForce : 0f;

        ApplyBreaking();
    }

    private void ApplyTorque(float torque, float drag)
    {
        if (activeRearWheels)
        {
            rearLeftWheelCollider.motorTorque = torque;
            rearRightWheelCollider.motorTorque = torque;
        }
        if (activeFrontWheels)
        {
            frontLeftWheelCollider.motorTorque = torque;
            frontRightWheelCollider.motorTorque = torque;
        }
        
        rb.drag = drag;
    }

    private void ApplyBreaking()
    {
        if (!isGoingBackWard && isBreaking) ApplyTorque(-breakForce, rbDragMalus);
        else 
        {
            frontRightWheelCollider.brakeTorque = currentbreakForce;
            frontLeftWheelCollider.brakeTorque = currentbreakForce;
            rearLeftWheelCollider.brakeTorque = currentbreakForce;
            rearRightWheelCollider.brakeTorque = currentbreakForce;
        }
    }

    private void HandleSteering()
    {
        currentSteerAngle = maxSteerAngle * horizontalInput;
        frontLeftWheelCollider.steerAngle = currentSteerAngle;
        frontRightWheelCollider.steerAngle = currentSteerAngle;
    }

    private void CheckRotationReset()
    {
        if (resetPosition)
        {
            transform.position = new Vector3(transform.position.x, transform.position.y + 0.5f, transform.position.z);
            transform.rotation = Quaternion.identity;
        }
    }
        
    private void UpdateDownForce()
    {
        if (rb != null)
        {
            float lift = liftCoefficient * rb.velocity.sqrMagnitude;
            rb.AddForceAtPosition(lift * transform.up, transform.position);
        }
    }

    private void UpdateDirection()
    {

        if (localVelocity.z < 0f)
        {
            isGoingBackWard = true;
        }
        else
        {
            isGoingBackWard = false;
        }
    }
    #endregion

    #region GRAPHIC_MANAGEMENT
    private void UpdateWheels()
    {
        UpdateSingleWheel(frontLeftWheelCollider, frontLeftWheelTransform);
        UpdateSingleWheel(frontRightWheelCollider, frontRightWheeTransform);
        UpdateSingleWheel(rearRightWheelCollider, rearRightWheelTransform);
        UpdateSingleWheel(rearLeftWheelCollider, rearLeftWheelTransform);
    }
    private void UpdateSingleWheel(WheelCollider wheelCollider, Transform wheelTransform)
    {
        Vector3 pos;
        Quaternion rot;
        wheelCollider.GetWorldPose(out pos, out rot);
        wheelTransform.rotation = rot;
        wheelTransform.position = pos;
    }

    private void UpdateBreakEffects()
    {
        foreach(Light bLight in breakLights)
        bLight.intensity = isBreaking ? breakLightIntensity : 0f;

        foreach(ParticleSystem ps in breakSmokeParticles)
        {
            if ((isBreaking)||(localVelocity.x > 0f)) ps.Play(); //if is breaking or drifting
            else ps.Stop();
        }
    }
    #endregion

    #region REAL_TIME_TUNING
    //set
    public void ActivateRearWheels(bool i_bool) { activeRearWheels = i_bool; } 
    public void ActivateFrontWheels(bool i_bool) { activeFrontWheels = i_bool; }
    public void SetTorqueForce(float i_torqueForce) { torqueForce = i_torqueForce; }
    public void SetMaxSpeed(float i_maxSpeed) { maxSpeed = i_maxSpeed; }
    public void SetTurboCoefficent(float i_coeffincent) { turboCoefficent = i_coeffincent; }
    public void SetBreakForce(float i_breakForce) { breakForce = i_breakForce; }
    public void SetDownForceCoefficent(float i_coefficent) { liftCoefficient = i_coefficent; }
    public void SetCarMass(float i_mass) { rb.mass = i_mass; }
    //get
    public bool GetRearWheelsStatus() { return activeRearWheels; }
    public bool GetFrontWheelsStatus() { return activeFrontWheels; }
    public float GetTorqueForce() { return torqueForce; }
    public float GetMaxSpeed() { return maxSpeed; }
    public float GetTurboCoefficent() { return turboCoefficent; }
    public float GetBreakForce() { return breakForce; }
    public float GetDownForceCoefficent() { return liftCoefficient; }
    public float GetCarMass() { return rb.mass; }
    #endregion

}