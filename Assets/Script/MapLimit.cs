using UnityEngine;
using UnityEngine.SceneManagement;

public class MapLimit : MonoBehaviour
{
    private const string PLAYER_TAG = "Player";
    [SerializeField] private Transform spawnPoint;
    [SerializeField] private Transform playerTransform;
    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag(PLAYER_TAG)) { SceneManager.LoadScene(SceneManager.GetActiveScene().name); }
    }
}
